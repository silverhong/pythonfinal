﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSolution.Entity
{
    public class EvaluationType
    {
        public string EvFId { get; set; }
        public string EvFName { get; set; }
    }
}
