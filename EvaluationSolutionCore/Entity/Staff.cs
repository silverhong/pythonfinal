﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSolution.Entity
{
    public class Staff
    {
        public string StaffId { get; set; }
        public string Sname { get; set; }
        public string Dob { get; set; }
        public string Pob { get; set; }
        public string CurrentAddress { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string DeptId { get; set; }
        public string OfficeId { get; set; }
        public string Pid { get; set; }
        public string Createdby { get; set; }
        public string Usergroup { get; set; }
    }
}
