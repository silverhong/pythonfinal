﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSolution.Entity
{
    public class VQuestion
    {
        public int No { get; set; } = 1;
        public string Description { get; set; }
    }
}
