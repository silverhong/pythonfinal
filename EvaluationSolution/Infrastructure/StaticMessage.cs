﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvaluationSolution.Infrastructure
{
    public static class StaticMessage
    {
        public static string CancelMessage = "Do you really want to cancel ?";
        public static string CancelRegisterTitle = "Cancel Registration";
        public static string CancelEditTitle = "Cancel Edit";
    }
}
