﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using EvaluationSolution.Infrastructure;
using EvaluationSolution.Entity;
using EvaluationSolution.UI.View;

namespace EvaluationSolution.UI.Forms
{
    public partial class MainForm : MetroForm
    {
        public MainForm()
        {
            InitializeComponent();
            using(LoginForm login = new LoginForm())
            {
                if (login.ShowDialog() == DialogResult.OK)
                {

                }
                else
                    Environment.Exit(1);
            }
        }
        private void Menu_Checked(object sender,EventArgs e)
        {
            string name = ((RadioButton)sender).Text.ToLower().ToString();
            var view = Singleton.Instance.Container.Resolve<MainView>(name);
            view.Dock = DockStyle.Fill;
            panelMain.Controls.Clear();
            panelMain.Controls.Add(view);
        }
        private void LbLoginAs_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }
    }
}
