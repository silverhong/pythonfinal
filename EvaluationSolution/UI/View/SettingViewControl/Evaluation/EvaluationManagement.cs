﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvaluationSolution.Entity;
namespace EvaluationSolution.UI.View.SettingViewControl.Evaluation
{
    public partial class EvaluationManagement : MainView
    {
        public EvaluationManagement()
        {
            InitializeComponent();
            List<Entity.Evaluation> listEva = new List<Entity.Evaluation>() {
                new Entity.Evaluation()
                { EvId = "001", EvDescription = "Staff Evaluation of SETEC", fromDate = "24/06/2019", toDate = "27/06/2019", CreatedDate = "24/06/2019", StaffId = "G1" }
                };
            dataGridMain.DataSource = listEva;
        }

        private void DataGridMain_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridMain.ClearSelection();
        }

        private void DataGridMain_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public override void Init()
        {
            dataGridMain.ClearSelection();
        }

        private void BtnAddEvaluation_Click(object sender, EventArgs e)
        {
            AddEvaluation addEvaluation = new AddEvaluation();
            addEvaluation.ShowDialog();
        }

        private void DataGridMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
