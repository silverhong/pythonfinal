﻿using EvaluationSolution.Entity;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSolution.UI.View.SettingViewControl.Evaluation.AddEvaluationControl
{
    public partial class SelectQuestion : MetroForm
    {
        public SelectQuestion()
        {
            InitializeComponent();
            List<Entity.EvaluationQuestion> listEvQ = new List<Entity.EvaluationQuestion>();
            for (int i = 0; i < 10; i++)
            {
                listEvQ.Add(new Entity.EvaluationQuestion()
                {
                    EvQId = "001",
                    CreatedDate = "24/06/2019",
                    EvQDescription = "How often is they come to work ?",
                    StaffId = "GG"
                });
            }
            int num = 1;
            List<VQuestion> vQuestion = listEvQ.Select(q =>
            {
                VQuestion question = new VQuestion()
                {
                    No = num,
                    Description = q.EvQDescription
                };
                num++;
                return question;
            }).ToList();
            dataGridMain.DataSource = vQuestion;
            var headercell = DataGridViewContentAlignment.MiddleCenter;
            dataGridMain.Columns.Add(new DataGridViewCheckBoxColumn() { HeaderText = "Selected", Name = "Selected", TrueValue = true, FalseValue = false});
            dataGridMain.Columns[dataGridMain.Columns.Count-1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            foreach(DataGridViewColumn col in dataGridMain.Columns)
            {
                if (col.HeaderText != "Selected")
                    col.ReadOnly = true;
            }

        }
        private void DataGridMain_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridMain.CurrentCell.RowIndex;
            bool? con = (bool?)dataGridMain["Selected", index].Value;
            if (con.HasValue && con.Value == true)
            {
                dataGridMain.Rows[index].Cells["Selected"].Value = false;
                dataGridMain.Rows[index].DefaultCellStyle.ForeColor = Color.Black;
            }
            else
            {
                dataGridMain.Rows[index].Cells["Selected"].Value = true;
                dataGridMain.Rows[index].DefaultCellStyle.ForeColor = Color.Red;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to cancel Staff Selection ?", "Staff Selection").DialogResult == DialogResult.OK)
            {
                this.Dispose();
            }
        }
    }
}
