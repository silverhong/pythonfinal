﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSolution.UI.View.SettingViewControl.Evaluation.AddEvaluationControl
{
    public partial class SelectStaff : MetroForm
    {
        public SelectStaff()
        {
            InitializeComponent();
            List<VStaff> listStaff = new List<VStaff>();
            for (int i = 1; i < 100; i++)
            {
                listStaff.Add(new VStaff()
                {
                    ID = i + "",
                    Name = "Kimhong",
                    Address = "Phnom Penh",
                    DateOfBirth = "06/06/1999",
                    Email = "kimhongvuthy@gmail.com",
                    Gender = "Male",
                    PlaceOfBirth = "Prey Veng",
                    Position = "IT Manager"
                }
                );
            }
            dataGridMain.DataSource = listStaff.Select(i => new { i.ID, i.Name, i.Position }).ToList();
            dataGridMain.Columns.Add(new DataGridViewCheckBoxColumn() { HeaderText = "Selected", Name = "Selected", TrueValue = true, FalseValue = false });
            foreach (DataGridViewColumn col in dataGridMain.Columns)
            {
                if (col.HeaderText != "Selected")
                    col.ReadOnly = true;
            }
        }
        private void DataGridMain_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridMain.CurrentCell.RowIndex;
            bool? con = (bool?)dataGridMain["Selected", index].Value;
            if (con.HasValue && con.Value == true)
            {
                dataGridMain.Rows[index].Cells["Selected"].Value = false;
                dataGridMain.Rows[index].DefaultCellStyle.ForeColor = Color.Black;
            }
            else
            {
                dataGridMain.Rows[index].Cells["Selected"].Value = true;
                dataGridMain.Rows[index].DefaultCellStyle.ForeColor = Color.Red;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to cancel Question Selection ?", "Question Selection").DialogResult == DialogResult.OK)
            {
                this.Dispose();
            }
        }
    }
}
