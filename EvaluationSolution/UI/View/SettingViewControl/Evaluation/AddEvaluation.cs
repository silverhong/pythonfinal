﻿using EvaluationSolution.UI.View.SettingViewControl.Evaluation.AddEvaluationControl;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSolution.UI.View.SettingViewControl.Evaluation
{
    public partial class AddEvaluation : MetroForm
    {
        public AddEvaluation()
        {
            InitializeComponent();
            List<Entity.EvaluationQuestion> listQ = new List<Entity.EvaluationQuestion>()
            {
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"},
                new Entity.EvaluationQuestion(){CreatedDate="27/06/2019",EvQDescription="How is your working ?",EvQId="001",StaffId="002"}
            };
            dataGridQuestion.DataSource = listQ;
            List<Entity.User> listU = new List<Entity.User>();
            for(int i = 1; i <= 30; i++)
            {
                listU.Add(new Entity.User() { ID = i, Name = $"User {i}" });
            }
            dataGridStaff.DataSource = listU;
        }
        private void AddEvaluation_Load(object sender, EventArgs e)
        {

        }

        private void LbCompanyName_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void MetroDateTime2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void MetroDateTime1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MetroLabel7_Click(object sender, EventArgs e)
        {

        }

        private void DataGridQuestion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MetroButton4_Click(object sender, EventArgs e)
        {

        }

        private void MetroButton1_Click(object sender, EventArgs e)
        {

        }

        private void BtnAddQuestion_Click(object sender, EventArgs e)
        {
            SelectQuestion selectQuestion = new SelectQuestion();
            selectQuestion.ShowDialog();
        }

        private void BtnAddStaff_Click(object sender, EventArgs e)
        {
            SelectStaff selectStaff = new SelectStaff();
            selectStaff.ShowDialog();
        }
    }
}
