﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvaluationSolution.UI.View.SettingViewControl.Evaluation
{
    public partial class EvaluationType : MainView
    {
        public EvaluationType()
        {
            InitializeComponent();
            List<Entity.EvaluationType> listEvT = new List<Entity.EvaluationType>();
            listEvT.Add(new Entity.EvaluationType() { EvFId = "001", EvFName = "Every Month" });
            dataGridMain.DataSource = listEvT;
        }
    }
}
