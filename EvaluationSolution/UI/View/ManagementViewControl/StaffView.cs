﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EvaluationSolution.Entity;
namespace EvaluationSolution.UI.View.ManagementViewControl
{
    public partial class StaffView : MainView
    {
        public StaffView()
        {
            InitializeComponent();
            List<VStaff> listStaff = new List<VStaff>();
            for (int i = 1; i < 100; i++)
            {
                listStaff.Add(new VStaff()
                    {
                        ID = i + "",
                        Name = "Kimhong",
                        Address = "Phnom Penh",
                        DateOfBirth = "06/06/1999",
                        Email = "kimhongvuthy@gmail.com",
                        Gender = "Male",
                        PlaceOfBirth = "Prey Veng",
                        Position="IT Manager"
                    }
                );
            }
            dataGridMain.DataSource = listStaff;
        }

        private void BtnAddStaff_Click(object sender, EventArgs e)
        {
            using (AddStaffForm add = new AddStaffForm())
            {
                add.ShowDialog();
            }
        }
        private void DataGridMain_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dataGridMain.ClearSelection();
        }
    }
}
